package au.edu.uts.aip.service;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

/** 
 * JAX-RS configuration to host JAX-RS web services under the /api/.
 */
@ApplicationPath("api")
public class ApplicationConfig extends Application {

}

