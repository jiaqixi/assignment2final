package au.edu.uts.aip.domain;

import ad.edu.uts.aip.data.*;
import java.util.*;
import javax.ejb.*;
import javax.persistence.*;
import javax.persistence.PersistenceContext;

/**
 * An FeedbackBean use Feedback JPA.
 * All operations are stateless.
 * Authored by JIA QI XI
 */
@Stateless
public class FeedbackBean {
    @PersistenceContext
    private EntityManager em;
    
    /**
     * create a feedback
     * @param feedback 
     */
    public void createFeedback(Feedback feedback) {
        em.persist(feedback);
    }
    
    /**
     * get a feedback by id
     * @param id
     * @return feedback
     */
    public Feedback getFeedback(int id) {
        return em.find(Feedback.class, id);
    }
    
    /**
     * delete a feedback
     * @param id 
     */
    public void deleteFeedback(int id) {
        Feedback feedback = em.find(Feedback.class, id);
        em.remove(feedback);
    }
    
    /**
     * update feedback
     * @param feedback 
     */
    public void updateFeedback(Feedback feedback) {
        em.merge(feedback);
    }
    
    /**
     * list feedbacks
     * @return 
     */
    public List<Feedback> listFeedbacks() {
        return em.createQuery("select f from Feedback f", Feedback.class).getResultList();
    }
}
