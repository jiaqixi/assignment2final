package au.edu.uts.aip.domain;

import ad.edu.uts.aip.data.*;
import java.util.*;
import javax.ejb.*;
import javax.persistence.*;

/**
 * An AccountBean use Account JPA.
 * All operations are stateless.
 */
@Stateless
public class AccountBean 
{
    @PersistenceContext
    private EntityManager em;
    
    /**
     * create a new account
     * @param account 
     */
    public void createAccount(Account account){
        em.persist(account); 
    }
    
    /**
     * find all accounts
     * @return 
     */
    public List<Account> findAllAccount() {
        return em.createQuery("select a from Account a", Account.class).getResultList();
    }
    
    /**
     * find an account by user
     * @param userName
     * @return Account
     */
    public Account getAccountByUserName(String userName){
        return em.find(Account.class, userName);
    }
    
    /**
     * update account
     * @param currentAccount 
     */
    public void updateAccount(Account currentAccount){
        em.merge(currentAccount);
    }
    
}
