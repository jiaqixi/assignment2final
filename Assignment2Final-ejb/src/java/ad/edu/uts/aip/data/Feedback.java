package ad.edu.uts.aip.data;

/*
 * This is Feedback JPA enetity.
 *JAXB-annotated class used for serialization and deserialization of feedback
 * use RESTful web service.
 * used to export the record in XML
 * Author By Jia Qi Xi
 */

import java.io.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Feedback implements Serializable
{
    private int id;
    private String subjectName;
    private String content;
    private String userName;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @NotNull(message = "subject name is required")
    @Size(min = 1, message = "subject name is required")
    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @NotNull(message = "Content is required")
    @Size(min = 1, message = "Content is required")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
}
