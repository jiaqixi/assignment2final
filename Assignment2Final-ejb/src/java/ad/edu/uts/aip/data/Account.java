package ad.edu.uts.aip.data;

import java.io.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An account includes userName, password and subscription.
 * UserName is primary key. userName, password and subscription both have get/set method
 * @author Jia Qi Xi
 */
@Entity
public class Account implements Serializable
{
    private String UserName;
    private String Password;
    private String Subscription;

    @Id
    @NotNull(message = "UserName is required")
    @Size(min = 1, message = "UserName is required") 
    public String getUserName(){
        return UserName;
    }

    public void setUserName(String UserName){
        this.UserName = UserName;
    }
   
    @NotNull(message = "Password is required")
    @Size(min = 1, message = "Password is required") 
    public String getPassword(){
        return Password;
    }

    public void setPassword(String Password){
        this.Password = Password;
    }

    @NotNull(message = "subscription is required")
    @Size(min = 1, message = "subscription is required")
    public String getSubscription(){
        return Subscription;
    }

    public void setSubscription(String Subscription){
        this.Subscription = Subscription;
    }
      
}
