package au.uts.edu.aip.controller;

import ad.edu.uts.aip.data.Account;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * SessionController is used to save the current user
 * @author JIA QI XI
 */
@Named
@SessionScoped
public class SessionController implements Serializable
{
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }   
}
