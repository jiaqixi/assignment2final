package au.uts.edu.aip.controller;

import au.edu.uts.aip.domain.FeedbackBean;
import ad.edu.uts.aip.data.Feedback;
import java.io.*;
import java.util.Collection;
import javax.ejb.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * This is FeedbackController used to call from FeedbackBean
 * account is used in path
 * @author Jia Qi Xi
 */
@Named
@RequestScoped
@Path("account")
public class FeedbackController implements Serializable {
    @EJB
    private FeedbackBean feedbackBean;   
    private Feedback feedback = new Feedback();

    public Feedback getFeedback() {
        return feedback;
    }

    /**
     * select feedback
     * @param id 
     */
    public void loadFeedback(int id){
        feedback = feedbackBean.getFeedback(id);
    }
    
    /**
     * delete feedback
     */
    public String deleteFeedback(){
        feedbackBean.deleteFeedback(feedback.getId());
        return "list?faces-redirect=true";     
    }
    
    /**
     * get feedbacks
     * @return collection of feedbacks
     */
    @GET
    public Collection<Feedback> getFeedbacks(){       
        return feedbackBean.listFeedbacks();
    }
    
    /**
     * Update selected record into feedback database
     * display warning message when either subject or content is null
     * @return list view when all the content are updated
     */
    public String saveChanges(){
        feedbackBean.updateFeedback(feedback);
        return "list?faces-redirect=true";
    }

    /**
     *Insert record into feedback database when both subject and feedback content are not null
     * @return different pages when the subscription is different
     */   
    public String save(SessionController sessionController){
        String currentUser = sessionController.getAccount().getUserName();
        feedback.setUserName(currentUser);             
        feedbackBean.createFeedback(feedback);
        
        if(sessionController.getAccount().getSubscription().equals("STANDARD"))
        {
            return "listStardard?faces-redirect=true";
        }
        else if(sessionController.getAccount().getSubscription().equals("PREMIUM"))
        {
            return "list?faces-redirect=true";
        }
        else
        {
            return null;  
        }
    }
}
