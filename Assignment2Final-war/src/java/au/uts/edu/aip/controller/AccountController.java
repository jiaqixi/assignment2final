package au.uts.edu.aip.controller;

import au.edu.uts.aip.domain.AccountBean;
import ad.edu.uts.aip.data.Account;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.ejb.*;
import javax.faces.application.*;
import javax.faces.context.*;
import javax.faces.view.ViewScoped;
import javax.inject.*;
import javax.servlet.*;

/**
 * AccountController is the presentation logic that call from accountBean
 * @author 17
 */
@Named
@ViewScoped
public class AccountController implements Serializable { 
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @EJB 
    private AccountBean accountBean;
    private Account account = new Account();

    public Account getAccount() {
        return account;
    }
    
    /**
     * get current account by sessionController
     * @param sessionController 
     */
    public void loadAccount(SessionController sessionController) {
        account=sessionController.getAccount();
    }
    
    /**
     * save new record into account database
     * get hashed password
     * insert record into account
     * display warning information
     */
    public String save( ) throws NoSuchAlgorithmException {
        //Check whether username exist      
        if(accountBean.getAccountByUserName(account.getUserName())!=null)
        {
            showError("UserName exists!");
            return null;
        }
        else
        {   
            //get hashed password and set
            String currentPassword= hash256(account.getPassword());         
            account.setPassword(currentPassword);
            accountBean.createAccount(account);       
            return "registersucceed?faces-redirect=true";            
        }
    }  
    
    /**
     * configure hash password
     * return hashed password 
     */
    public static String hash256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(data.getBytes());
        return bytesToHex(md.digest());
    }
    
   /**
    * get hash value
    * @param bytes
    * @return 
    */
    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
        {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }
    
    /**
     * Adds a message to the current faces context, so that it will appear in
     * @param message the text of the error message to show the user
     */
    private void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
    
    /**
    * List all the accounts
    * @return list
    */
    public List<Account> getPeople() {
        return accountBean.findAllAccount(); 
    }
    
    /**
     * login method,users can log in 
     * @param sessionController
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public String login(SessionController sessionController) throws NoSuchAlgorithmException  {
         //get the current account
         Account currentAccount=accountBean.getAccountByUserName(userName);
         
         //differnt subscription user can go into different page
         if(currentAccount!=null&&currentAccount.getPassword().equals(hash256(password)))
         {
             sessionController.setAccount(currentAccount);
             
             if(currentAccount.getSubscription().equals("FREE"))
             {
                  return "/listFree?faces-redirect=true";
             }
             else if (currentAccount.getSubscription().equals("STANDARD"))
             {
                 return "/listStardard?faces-redirect=true";
             }
             else if (currentAccount.getSubscription().equals("PREMIUM"))
             {
                 return "/list?faces-redirect=true";
             }
             else
                 return "/listAdmin?faces-redirect=true";
         }
         else
         {
             //show error when log in 
             showError("Incorrect username or password (or you may not have properly configured aipRealm)");
             showError("Click log out to re-log in ");  
             return null;
         }     
     
     }
    
    /**
     * update changes after editing it
     * @return to different pages according to subscription
     */
    public String saveChanges() {
        accountBean.updateAccount(account);
        if(account.getSubscription().equals("FREE"))
        {
            return "listFree?faces-redirect=true";
        }
        else if(account.getSubscription().equals("STANDARD"))
        {
            return "listStardard?faces-redirect=true";
        }
        else if(account.getSubscription().equals("PREMIUM"))
        {
            return "list?faces-redirect=true";
        }
        else
        {
            return null;
        }            
   }

    /**
     * Logs out the current user 
     * current user in the given session controller.
     * @param sessionController
     * @throws ServletException 
     */
    public String logout(SessionController sessionController) throws ServletException  {
        sessionController.setAccount(null);
        return "/login?faces-redirect=true";
    }
   
}
